{
  description = "Your new nix config";

  inputs = {
    # Nixpkgs
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    devenv.url = "github:cachix/devenv/v0.4";

    # Home manager
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    # TODO: Add any other flake you might need
    hardware.url = "github:nixos/nixos-hardware";
    emacs.url = "github:nix-community/emacs-overlay/master";
    mach-nix.url = "github:DavHau/mach-nix";

    vscode-server.url = "github:nix-community/nixos-vscode-server";
    
  };

  outputs = { nixpkgs, home-manager, devenv, vscode-server, ... }@inputs: {
    nixosConfigurations = {
      tyler-desk = nixpkgs.lib.nixosSystem {
        specialArgs = { inherit inputs; }; # Pass flake inputs to our config
        # > Our main nixos configuration file <
        modules = [
          vscode-server.nixosModules.default
          ./nixos/configuration.nix
        ];
      };
    };

    homeConfigurations = {
      "tyler@tyler-desk" = home-manager.lib.homeManagerConfiguration {
        pkgs = nixpkgs.legacyPackages.x86_64-linux; # Home-manager requires 'pkgs' instance
        extraSpecialArgs = { inherit inputs; }; # Pass flake inputs to our config
        # > Our main home-manager configuration file <
        modules = [ ./home-manager/home.nix ./home-manager/linux ];
      };
      "tyler@tyler-macbook" = home-manager.lib.homeManagerConfiguration {
        pkgs = nixpkgs.legacyPackages.aarch64-darwin; # Home-manager requires 'pkgs' instance
        extraSpecialArgs = { inherit inputs; }; # Pass flake inputs to our config
        # > Our main home-manager configuration file <
        modules = [ ./home-manager/home.nix ./home-manager/macos ];
      };
    };
  };
}
