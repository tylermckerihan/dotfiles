{ pkgs, ... }:

{
  home.packages = with pkgs; [
    networkmanagerapplet  # for nm-connection-editor
    docker-compose
    brave
    teams
    flatpak
    dbeaver
    vlc
    rbenv
  ];
}
