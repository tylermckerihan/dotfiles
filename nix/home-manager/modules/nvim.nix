{ pkgs, config, ... }:

{
  programs.neovim = {
    enable = true;
    extraConfig = ''
      set termguicolors
      set number
      set smartindent
      set expandtab
      set tabstop=2
      set shiftwidth=2

      #nnoremap <c-p> :FZF<cr>

      syntax enable
      #colorscheme tokyonight
    '';
  };
}
      
