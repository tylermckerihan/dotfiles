{ ... }:

{
  # gh auth login　時 readonly のエラー出るけど問題なし
  # https://github.com/cli/cli/issues/4955
  programs.gh = {
    enable = true;
    gitCredentialHelper.enable = true;
  };

  programs.git = {
    enable = true;

    userName = "Tyler McKerihan";

    extraConfig = {
      core.editor = "nvim";
      credential.helper = "store";
    };
  };
}

