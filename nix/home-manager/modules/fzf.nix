{ pkgs, config, ... }:

{
  programs.fzf = {
    enable = true;
    defaultCommand = "rg --files"
  }
}
