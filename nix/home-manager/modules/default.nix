{ ... }:

{
  imports = [
    ./apps.nix
    ./git.nix
    ./tmux.nix
    ./emacs.nix
    ./kitty.nix
    ./fish.nix
    ./qemu.nix
    ./vscode.nix
  ];
}
