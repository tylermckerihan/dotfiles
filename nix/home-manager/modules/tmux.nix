{ pkgs, config, ... }:

{
  programs.tmux = {
    enable = true;
    shortcut = "a";
    baseIndex = 1;
    escapeTime = 0;
    keyMode = "vi";

    extraConfig =  ''
      # Some tweaks to the status line
      set -g status-right "%H:%M"
      
      # If running inside tmux ($TMUX is set), then change the status line to red
      %if #{TMUX}
      set -g status-bg red
      %endif
      
      # Enable RGB colour if running in xterm(1)
      set-option -sa terminal-overrides ",xterm*:Tc"
      
      # Change the default $TERM to tmux-256color
      #set -g default-terminal "tmux-256color"
      set -g default-terminal xterm-kitty
      
      # No bells at all
      set -g bell-action none
      
      # Turn the mouse on, but without copy mode dragging
      set -g mouse on
      
    #  # Keys to toggle monitoring activity in a window, and synchronize-panes
    #  bind m set monitor-activity
    #  bind y set synchronize-panes\; display 'synchronize-panes #{?synchronize-panes,on,off}'
    #  
    #  # vim panel switching
    #  #bind k selectp -U # switch to panel Up
    #  #bind j selectp -D # switch to panel Down 
    #  #bind h selectp -L # switch to panel Left
    #  #bind l selectp -R # switch to panel Right
    #  
    #  
      # smart pane switching with awareness of vim splits
      bind -n C-h run "(tmux display-message -p '#{pane_current_command}' | grep -iq vim && tmux send-keys C-h) || tmux select-pane -L"
      bind -n C-j run "(tmux display-message -p '#{pane_current_command}' | grep -iq vim && tmux send-keys C-j) || tmux select-pane -D"
      bind -n C-k run "(tmux display-message -p '#{pane_current_command}' | grep -iq vim && tmux send-keys C-k) || tmux select-pane -U"
      bind -n C-l run "(tmux display-message -p '#{pane_current_command}' | grep -iq vim && tmux send-keys C-l) || tmux select-pane -R"
      '';
    
  };
}
      
