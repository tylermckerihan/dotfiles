{ pkgs, ... }:

{
  home.packages = with pkgs; [
    gnumake
    terraform
    awscli
    #azure-cli
    colima
    clojure
    fd
    obsidian
    ripgrep
    jdk11
  ];
}
