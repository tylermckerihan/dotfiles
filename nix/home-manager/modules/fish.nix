{ config, pkgs, ... }:

{
  programs.fish = {
    enable = true;
    shellInit = ''
    #  fenv source '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh'
      export PATH="$PATH":"$HOME/.emacs.d/bin":"$HOME/.npm-global"
      export PATH="/opt/homebrew/opt/libpq/bin:$PATH"
      set PATH $HOME/.cargo/bin $PATH
      status --is-interactive; and rbenv init - fish | source
    '';
    plugins = [
      {
        name = "plugin-foreign-env";
        src = pkgs.fetchFromGitHub {
          owner = "oh-my-fish";
          repo = "plugin-foreign-env";
          rev = "dddd9213272a0ab848d474d0cbde12ad034e65bc";
          sha256 = "00xqlyl3lffc5l0viin1nyp819wf81fncqyz87jx8ljjdhilmgbs";
        };
      }
    ];
  };
}
