{ pkgs, ... }:

{
  home.packages = with pkgs; [
    colima
    tailscale
    docker
  ];
}
